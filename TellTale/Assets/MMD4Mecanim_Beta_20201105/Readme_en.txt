﻿----------------------------------------------------------------------------------------------------------------------------------------------------------------

MMD4Mecanim 2020/11/05 Beta

by Nora
http://stereoarts.jp

----------------------------------------------------------------------------------------------------------------------------------------------------------------

Notice:

A user has been abusing this software and posing a threat to me.
If there is no improvement, I will discontinue all support.

And as I've written many times, please don't upload to github.

----------------------------------------------------------------------------------------------------------------------------------------------------------------

/ About MMD4Mecanim

- This is a converter & Unity scripts for MMD models and motions.

/ Caution

- You must confirm with the terms of MMD models and motions.
- In most cases, it's not admitted without MMD. You must obtain permission from authors.

- Please observe morals.
  Don't make for erotic contents.
  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 (If you find it, please ask author & delete it.)

- If you want to use works by / under Crypton Future Media, you should check guideline at first.
  http://piapro.net/en_for_creators.html

/ Redistribution

- Don't upload model, motion, source codes without author's permission to public space.
(github, Dropbox, Blog, SNS, iTunes, Google Play, and anywhere.)

/ About supported platforms.

Now support platforms are Windows, Mac, WSA(UWP), iOS, and Android.

/ About Android platform support.

I've added the limitation for Android build. So, Now supported the connected Android device only on building APK on Unity.
It's a lock for uploading without permission by authors to Google Play, Dropbox, ...
You can't unlock this limitation. So you can't share the unlock technique via Blog, SNS, ...

/ About unsupported platforms.

- I've locked & denied Unsupported platform support on MM4Mecanim. (WebPlayer, WebGL, anythingelse)
- You can't unlock it with modify or hacking.
- You can't upload it to public spaces. (github, Dropbox, SNS, and anywhere.)

/ About publish application for iTunes, Google Play or otherwise.

Don't publish application without permission, regardless of commercial and non-commercial.
If you want to publish to iTunes, Google Play or otherwise, please require permission to all authors.
So you should confirmation legal responsibility.

/ Abount VR Chat

Don't use converted model & motion data for VR Chat.
Most models are prohibited to redistribution (or uploading).
If you've uploaded some models, please delete some models.

/ About converted model & motion by PMX2FBX

Don't use converted model & motion data out of Unity because those don't work material settings / morph without Unity scripts.

Don't use converted model & motion data for 3d print.

/ About modify & disassemble.

Don't modify & disassemble all exe, dll, and libraries in package.

/ Disclaimer

I do not take all responsibility about the damage by having used this software or components.

/ Supplement

The above restriction also occurs in past versions as well.

----------------------------------------------------------------------------------------------------------------------------------------------------------------

■ Licenses

FBX SDK 2016.1

This software contains Autodesk® FBX® code developed by Autodesk, Inc. Copyright 2016 Autodesk, Inc. All rights, reserved. Such code is provided “as is” and Autodesk, Inc. disclaims any and all warranties, whether express or implied, including without limitation the implied warranties of merchantability, fitness for a particular purpose or non-infringement of third party rights. In no event shall Autodesk, Inc. be liable for any direct, indirect, incidental, special, exemplary, or consequential damages (including, but not limited to, procurement of substitute goods or services; loss of use, data, or profits; or business interruption) however caused and on any theory of liability, whether in contract, strict liability, or tort (including negligence or otherwise) arising in any way out of such code.”

See more.
http://help.autodesk.com/view/FBX/2016/ENU/



Bullet Physics

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this software.
Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it freely,
subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.

See more.
http://bulletphysics.org



MeCab 0.996

Copyright (c) 2001-2008, Taku Kudo
Copyright (c) 2004-2008, Nippon Telegraph and Telephone Corporation
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are
permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above
   copyright notice, this list of conditions and the
   following disclaimer.

 * Redistributions in binary form must reproduce the above
   copyright notice, this list of conditions and the
   following disclaimer in the documentation and/or other
   materials provided with the distribution.

 * Neither the name of the Nippon Telegraph and Telegraph Corporation
   nor the names of its contributors may be used to endorse or
   promote products derived from this software without specific
   prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

See also,
http://taku910.github.io/mecab/

----------------------------------------------------------------------------------------------------------------------------------------------------------------
