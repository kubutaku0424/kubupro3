﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyCollider : MonoBehaviour
{

        // 他のオブジェクトとのすり抜け開始
        void OnTriggerEnter(Collider other)
        {
        // 処理
        SceneManager.LoadScene("BattleScene");

    }

}
